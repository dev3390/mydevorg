public class previewReportCtrlNew {
    public List<Lead> sobjList{get;set;}
    public List<List<Lead>> sobjList2{get;set;}
    public List<string> allLabels {get;set;}
    public List<string> allApiNames {get;set;}
    public date todayDate{get;set;}
    public string CampaignName{get;set;}
    public boolean CampaignFlag{get;set;}
    public boolean previewFlag;
    
    public previewReportCtrlNew(){
        CampaignFlag = false;
        previewFlag = true;
        if(ApexPages.currentPage().getParameters().get('scheduleFlag') == 'True'){
            previewFlag = false;
        }
        sobjList2 = new List<List<Lead>>();
        Id schId = ApexPages.currentPage().getParameters().get('schId');
        Schedule_Report__c schReport = [Select Id,Selected_Fields__c,Labels__c,FilterString__c from Schedule_Report__c where Id = :schId];
        
        sobjList2 = New List<List<Lead>>();
        allLabels = New List<string>();
        allApiNames = New List<string>();
        
        for(string field : schReport.Selected_Fields__c.split(',')){
            system.debug('field');
            string obj = field.split(':')[0].trim();
            string fieldNew = field.split(':')[1].trim();
            system.debug(obj);
            system.debug(fieldNew);
            
            Map <String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get('Lead').getDescribe().fields.getMap();
            Map <String, Schema.SObjectField> fieldMap1 = Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap();
            Map <String, Schema.SObjectField> fieldMap2 = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap();
            Map <String, Schema.SObjectField> fieldMap3 = Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap();
            
            if(obj == 'Lead'){
                allLabels.add(fieldMap.get(fieldNew).getDescribe().getlabel());
                allApiNames.add(fieldNew);
            }else if(obj == 'Account'){
                allLabels.add(fieldMap1.get(fieldNew).getDescribe().getlabel());
                allApiNames.add('convertedaccount.'+fieldNew);
            }else if(obj == 'Contact'){
                allLabels.add(fieldMap2.get(fieldNew).getDescribe().getlabel());
                allApiNames.add('convertedContact.'+fieldNew);
            }else if(obj == 'Opportunity'){
                allLabels.add(fieldMap3.get(fieldNew).getDescribe().getlabel());
                allApiNames.add('convertedOpportunity.'+fieldNew);
            }
        }
            
        string fields = '';
        for(string apiName :allApiNames){
            fields += apiName+ ',';
        }
        fields = fields.substring(0,fields.length()-1);
        
        string query = '';
        if(schReport.FilterString__c != null && schReport.FilterString__c != ''){
            query = 'Select '+fields+' From Lead '+schReport.FilterString__c;
        }else{
            query = 'Select '+fields+' From Lead';
        }
        
        if(previewFlag){
            query += ' LIMIT 20';
        }
        
        todayDate = date.today();
       
        sobjList = Database.query(query);
       
        Integer j = 0;
        integer k = 0;
        List<Lead> thList = new List<Lead>();
        
        for(Integer i=0; i < sobjList.size(); i++) {
           thList.add(sobjList[i]);
           if(thList.size() == 5000){
               sobjList2.add(thList);
               thList = new List<Lead>();
           }
        }
        
        if(thList.size() > 0){
            sobjList2.add(thList);
           thList = new List<Lead>();
        }
        List<CampaignMember> cam = [SELECT CampaignId,Campaign.name FROM CampaignMember where LeadId  IN :sobjList];
        if(cam.size() > 0){
            if(cam[0].CampaignId != null){
                CampaignFlag = true;
                CampaignName = cam[0].Campaign.name;
            }
        }
    }
}