public class allFields {
    
     public allFields(){
        string allField = '';
        Map <String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap();
        for(string field : fieldMap.keyset()){
             Schema.DisplayType fielddataType = fieldMap.get(field).getDescribe().getType();
             if(fielddataType != Schema.DisplayType.Address){
                //fieldList.add(new SelectOption(field, string.valueOf(fieldMap.get(field))));
                allField += string.valueOf(fieldMap.get(field)) + ',';
             }
             
        }
        allField = allField.substring(0, allField.length() -1);
        string queryString = 'Select '+allField+' From Contact';
        system.debug(Database.query(queryString));
    }

}