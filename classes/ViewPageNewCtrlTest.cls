@isTest
private class ViewPageNewCtrlTest {

  @isTest private static void test1() {
   
   Test.startTest();
   Schedule_Report__c   std=new Schedule_Report__c();
   std.Report_Name__c='some';
   
   insert std;
   
   PageReference pref = Page.ViewPageNew;
   pref.getParameters().put('id', std.id);
   
   ApexPages.StandardController cs1 = new ApexPages.standardController(std);
   ViewPageNewCtrl obj = new ViewPageNewCtrl(cs1);
   obj.redirect();
   Test.stopTest();

}
@isTest private static void test2() {
   
   Test.startTest();
   Schedule_Report__c   std=new Schedule_Report__c();
   std.Report_Name__c='some';
  
   PageReference pref = Page.ViewPageNew;
   pref.getParameters().put('id', std.id);
   
   ApexPages.StandardController cs1 = new ApexPages.standardController(std);
   ViewPageNewCtrl obj = new ViewPageNewCtrl(cs1);
   ViewPageNewCtrl obj1 = new ViewPageNewCtrl();
   obj.redirect();
   Test.stopTest();
}

}