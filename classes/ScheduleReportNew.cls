global class ScheduleReportNew implements Schedulable{
    
    global Set<String> emailList;
    global string schId;
    global blob pdfBlob;
    
    global void execute(SchedulableContext SC) {
        if(emailList.size() > 0){
            List<string> emailIds = new List<string>();
            emailIds.addAll(emailList);
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(emailIds);
            mail.setSubject('Report Generated');
            mail.setBccSender(false);
            mail.setUseSignature(false);
            mail.setPlainTextBody('Please find Report enclosed with this email.');
            
            PageReference pg = new PageReference('/apex/PreviewReportNew?schId='+schId+'&scheduleFlag=True');
            if(!Test.isRunningTest()){
                pdfBlob = pg.getContent();
            }else{
                pdfBlob = blob.valueOf('FileAttachment');
            }            
            List<Attachment> attachmentList  = [select Name, Body, BodyLength from Attachment where ParentId = :schId];
            if(attachmentList.size() > 0){
                List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                efa.setFileName('Report - ('+Datetime.now().formatGMT('yyyy-MM-dd')+')' + '.pdf');
                efa.setBody(attachmentList[0].body);
                fileAttachments.add(efa);
                
                mail.setFileAttachments(fileAttachments);
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            }
        }
    }
    public ScheduleReportNew (){
    }
    
    public ScheduleReportNew (Set<String> emailids, string schId) {
        emailList = emailids;
        this.schId = schId;
    }
    
}