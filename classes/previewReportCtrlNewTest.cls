@isTest
public class previewReportCtrlNewTest {
    
    @isTest public static void TestForLeadCase() {
        Schedule_Report__c std= new Schedule_Report__c();
        std.Report_Name__c='something';
        std.Selected_Fields__c='Lead: Name, Lead: city, Lead: company';
        insert std;
        
        Lead objLead= new Lead();
        objLead.Company='celebal';
        objLead.Status='No Contact';
        objLead.lastname='test';
        insert objLead;
        
        Campaign ObjCampagin =new Campaign();
        ObjCampagin.name='testData';
        insert ObjCampagin;
        
        CampaignMember ObjCampaginMem =new CampaignMember();
        ObjCampaginMem.CampaignId=ObjCampagin.id;
        ObjCampaginMem.Leadid=objLead.id;
        insert ObjCampaginMem;
        
        System.assertEquals(ObjCampaginMem,ObjCampaginMem);
        
        PageReference pref = Page.previewReportNew;
        ApexPages.currentPage().getParameters().put('schId',string.valueof(std.id));
        ApexPages.StandardController sc = new ApexPages.StandardController(std);
        
        previewReportCtrlNew prcn = new previewReportCtrlNew();
        
    }
    
    @isTest public static void TestForAccountCase() {
        Schedule_Report__c std= new Schedule_Report__c();
        std.Report_Name__c='something';
        std.Selected_Fields__c='Account: name,Account: Phone';
        insert std;
        
        Lead objLead= new Lead();
        objLead.Company='celebal';
        objLead.Status='No Contact';
        objLead.lastname='test';
        insert objLead;
        
        Campaign ObjCampagin =new Campaign();
        ObjCampagin.name='testData';
        insert ObjCampagin;
        
        CampaignMember ObjCampaginMem =new CampaignMember();
        ObjCampaginMem.CampaignId=ObjCampagin.id;
        ObjCampaginMem.Leadid=objLead.id;
        insert ObjCampaginMem;
        
        PageReference pref = Page.previewReportNew;
        ApexPages.currentPage().getParameters().put('schId',string.valueof(std.id));
        ApexPages.StandardController sc = new ApexPages.StandardController(std);
        
        previewReportCtrlNew prcn = new previewReportCtrlNew();
        
    }
     @isTest public static void TestForContactCase() {
        Schedule_Report__c std= new Schedule_Report__c();
        std.Report_Name__c='something';
        std.Selected_Fields__c='Contact:Lastname,Contact: Phone,Contact: Email';
        insert std;
        
        Lead objLead= new Lead();
        objLead.Company='celebal';
        objLead.Status='No Contact';
        objLead.lastname='test';
        insert objLead;
        
        Campaign ObjCampagin =new Campaign();
        ObjCampagin.name='testData';
        insert ObjCampagin;
        
        CampaignMember ObjCampaginMem =new CampaignMember();
        ObjCampaginMem.CampaignId=ObjCampagin.id;
        ObjCampaginMem.Leadid=objLead.id;
        insert ObjCampaginMem;
        
        PageReference pref = Page.previewReportNew;
        ApexPages.currentPage().getParameters().put('schId',string.valueof(std.id));
        ApexPages.StandardController sc = new ApexPages.StandardController(std);
        previewReportCtrlNew prcn = new previewReportCtrlNew();
        
    }
}