public class ReportTriggerHandler {
    public static void beforeDelete(List<Schedule_Report__c> newList){
        abortJobs(newList);
    }
    
    public static void abortJobs(List<Schedule_Report__c> newList){
        List<Schedule_Job__c> jobList = [Select id,Scheduled_Job_Id__c from Schedule_Job__c where Schedule_Report__c in:Newlist];
        for(Schedule_Job__c job :jobList){
            if(job.Scheduled_Job_Id__c != null){
                try{
                    System.abortJob(job.Scheduled_Job_Id__c);
                }catch(exception e){
                    
                }
            }
        }
    }
}