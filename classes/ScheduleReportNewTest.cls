@isTest
public class ScheduleReportNewTest {
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    @isTest private static void test() {
        
        // Test record of Schedule Report 
        Schedule_Report__c   scheduleReport = new Schedule_Report__c();
        scheduleReport.Report_Name__c='Test Report';
        scheduleReport.Selected_Fields__c = 'Lead: assign_to__c,Lead: city,Lead: comments__c,Lead: company';
        Insert scheduleReport;
        
        Attachment attach = new Attachment(); 
        attach.Body = Blob.valueOf('Test Data');
        attach.Name = Datetime.now().format('yyyy-MM-dd HH:mm')  + '.pdf';
        attach.IsPrivate = false;
        attach.ParentId = scheduleReport.Id;
        insert attach;
        
        System.assertEquals('Test Report',scheduleReport.Report_Name__c);
        
        Set<String> setOfEmail= new Set<String>();
        setOfEmail.add('test@gmail.com');   
        
        // Constructor call
        ScheduleReportNew scheduleObject = new ScheduleReportNew();
        System.schedule('Job - ('+Datetime.now().formatGMT('yyyy-MM-dd HH:mm:ss')+')', CRON_EXP, new ScheduleReportNew(setOfEmail,string.valueOf(scheduleReport.Id)));
    }
}