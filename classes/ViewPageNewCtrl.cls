public class ViewPageNewCtrl {
    public string schId; 
    
    public ViewPageNewCtrl(ApexPages.standardController std){
       Schedule_Report__c sch = (Schedule_Report__c)std.getRecord();
       schId = sch.id;
    }
    
    public ViewPageNewCtrl(){}
    
    public PageReference redirect(){
        PageReference pg;
        if(schId != '' && schId != null){
            pg = new PageReference('/apex/ReportGenerationNew?EditFlag=true&schId='+schId);
            
        }else{
            pg = new PageReference('/apex/ReportGenerationNew');
        }
        pg.setRedirect(true);
            return pg;
    }

}