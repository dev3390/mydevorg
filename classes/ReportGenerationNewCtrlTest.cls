@isTest
public class ReportGenerationNewCtrlTest 
{
    public static String CRON_EXP = '0 0 0 3 9 ? 2022';
    Static testMethod void Case1()
    {
        List<Schedule_Report__c> ObjSchReport= new list<Schedule_Report__c>();
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user01 = new User(Alias = 'Test001', Email='standarduser@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testlast', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = p1.Id, 
                               TimeZoneSidKey='America/Los_Angeles', UserName='user01@testorg.com');
        insert user01;
        
        Schedule_Report__c ObjReport = new Schedule_Report__c();
        ObjReport.Report_Name__c='Test';
        ObjReport.Selected_Fields__c='Lead: lastname';
        ObjReport.Labels__c='Labels';
        insert ObjReport;
        
        Schedule_Job__c schJob  = new Schedule_Job__c();
        schJob.Email_Ids__c = 'test@gmail.com';
        schJob.Schedule_Frequency__c = 'testFrequency';
        schJob.Scheduled_Job_Id__c = 'test123';
        schJob.Job_Name__c = 'testName';
        schJob.Next_Scheduled_Run__c = system.today();
        schJob.Submitted_By__c  = user01.id;
        schJob.Schedule_Report__c = ObjReport.id;
        insert schJob;
        
        PageReference pageRef = Page.ReportGenerationNew;
        ApexPages.currentPage().getParameters().put('EditFlag','true');
        ApexPages.currentPage().getParameters().put('schId',ObjReport.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(ObjReport);
        ReportGenerationNewCtrl ObjClass = new  ReportGenerationNewCtrl(sc);
        ObjClass.getFrequencyList();
        ObjClass.getWeekList();
        ObjClass.getTimeList();
        ObjClass.getDayList();
        ObjClass.getDayNumberList();
        ObjClass.getDays();
        ObjClass.FrequencyChange();
        
        
    }
    
    Static testMethod void Case2()
    {
        List<Schedule_Report__c> ObjSchReport= new list<Schedule_Report__c>();
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user01 = new User(Alias = 'Test001', Email='standarduser@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testlast', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = p1.Id, 
                               TimeZoneSidKey='America/Los_Angeles', UserName='user01@testorg.com');
        insert user01;
        
        Schedule_Report__c ObjReport = new Schedule_Report__c();
        ObjReport.Report_Name__c='Test';
        ObjReport.Selected_Fields__c='Lead: lastname';
        ObjReport.Labels__c='Labels';
        insert ObjReport;
        
        Schedule_Job__c schJob  = new Schedule_Job__c();
        schJob.Email_Ids__c = 'test@gmail.com';
        schJob.Schedule_Frequency__c = 'testFrequency';
        schJob.Scheduled_Job_Id__c = 'test123';
        schJob.Job_Name__c = 'testName';
        schJob.Next_Scheduled_Run__c = system.today();
        schJob.Submitted_By__c  = user01.id;
        schJob.Schedule_Report__c = ObjReport.id;
        insert schJob;
        
        PageReference pageRef = Page.ReportGenerationNew;
        ApexPages.currentPage().getParameters().put('ScheduleFlag','true');
        ApexPages.currentPage().getParameters().put('EditFlag','true');
        ApexPages.currentPage().getParameters().put('schId',ObjReport.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(ObjReport);
        ReportGenerationNewCtrl ObjClass = new  ReportGenerationNewCtrl(sc);
        ObjClass.getFrequencyList();
        ObjClass.getWeekList();
        ObjClass.getTimeList();
        ObjClass.getDayList();
        ObjClass.getDayNumberList();
        ObjClass.getDays();
        ObjClass.FrequencyChange();
          ObjClass.getFrequencyList().get(1);
        system.debug('SELECTFriqncy======'+ObjClass.getFrequencyList().get(0));
        
        ObjClass.getDayList().get(1);
        ObjClass.getTimeList().get(0);
        ObjClass.emailIds='test@gmail.com';
        ObjClass.selectedUsers=user01.id;
        //ObjClass.getDays().get(0);
        system.debug('====getDays====='+ ObjClass.Days);
        ObjClass.getWeekList().get(1);
        ObjClass.getWeekList().get(3);
        //ObjClass.getDayNumberList().get(1);
        system.debug('===days==='+ObjClass.days);
        objclass.back();
        ObjClass.schedule();
        
        
    }
    
    Static testMethod void Case3()
    {
        List<Schedule_Report__c> ObjSchReport= new list<Schedule_Report__c>();
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user01 = new User(Alias = 'Test001', Email='standarduser@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testlast', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = p1.Id, 
                               TimeZoneSidKey='America/Los_Angeles', UserName='user01@testorg.com');
        insert user01;
        
        Schedule_Report__c ObjReport = new Schedule_Report__c();
        ObjReport.Report_Name__c='Test';
        ObjReport.Selected_Fields__c='Lead: lastname,Account: name,Account: Phone,Lead: city,Lead: company,Contact:Lastname,Contact: Phone,Contact: Email,Lead: description';
        ObjReport.Labels__c='Labels';
        insert ObjReport;
        
        Schedule_Job__c schJob  = new Schedule_Job__c();
        schJob.Email_Ids__c = 'test@gmail.com';
        schJob.Schedule_Frequency__c = 'testFrequency';
        schJob.Scheduled_Job_Id__c = 'test123';
        schJob.Job_Name__c = 'testName';
        schJob.Next_Scheduled_Run__c = system.today();
        schJob.Submitted_By__c  = user01.id;
        schJob.Schedule_Report__c = ObjReport.id;
        insert schJob;
        delete schJob;
        
        PageReference pageRef = Page.ReportGenerationNew;
        ApexPages.currentPage().getParameters().put('EditFlag','true');
        ApexPages.currentPage().getParameters().put('schId',ObjReport.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(ObjReport);
        ReportGenerationNewCtrl ObjClass = new  ReportGenerationNewCtrl(sc);
        ObjClass.getFrequencyList();
        ObjClass.getWeekList(); 
        ObjClass.getTimeList();
        ObjClass.getDayList();
        ObjClass.getDayNumberList();
        ObjClass.getDays();
        ObjClass.FrequencyChange();
        ObjClass.Next1();
        Objclass.cancel();
        ObjClass.deleteRecord();
        
        
        // ObjClass.moveRight(ObjClass.selectedFieldList);
        
    }
    Static testMethod void Case4()
    {
        List<Schedule_Report__c> ObjSchReport= new list<Schedule_Report__c>();
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user01 = new User(Alias = 'Test001', Email='standarduser@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testlast', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = p1.Id, 
                               TimeZoneSidKey='America/Los_Angeles', UserName='user01@testorg.com');
        insert user01;
        
        Schedule_Report__c ObjReport = new Schedule_Report__c();
        ObjReport.Report_Name__c='Test';
        ObjReport.Selected_Fields__c='Lead: lastname,Account: name,Account: Phone, Opportunity: Name, Contact: FirstName';
        ObjReport.Labels__c='Labels';
        ObjSchReport.add(ObjReport);
        insert ObjSchReport;
        
        Schedule_Job__c schJob  = new Schedule_Job__c();
        schJob.Email_Ids__c = 'test@gmail.com';
        schJob.Schedule_Frequency__c = 'testFrequency';
        schJob.Scheduled_Job_Id__c = 'test123';
        schJob.Job_Name__c = 'testName';
        schJob.Next_Scheduled_Run__c = system.today();
        schJob.Submitted_By__c  = user01.id;
        schJob.Schedule_Report__c = ObjReport.id;
        insert schJob;
        
        PageReference pageRef = Page.ReportGenerationNew;
        ApexPages.currentPage().getParameters().put('rowIndex1',string.valueOf(0));
        ApexPages.currentPage().getParameters().put('EditFlag','true');
        ApexPages.currentPage().getParameters().put('schId',ObjReport.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(ObjReport);
        ReportGenerationNewCtrl ObjClass = new  ReportGenerationNewCtrl(sc);
        System.debug(ObjClass.fieldList);
        System.debug(ObjClass.selectedFieldList);
        ObjClass.selectedFields.add(ObjClass.selectedFieldList[1].getvalue());
        ObjClass.moveLeft();
        ObjClass.moveUp();
        ObjClass.moveDown();
        ObjClass.moveTop();
        ObjClass.moveBottom();
        // ObjClass.deselectedFields.add(ObjClass.fieldList[0].getvalue());
        //ObjClass.deselectedFields.add(ObjClass.fieldList[1].getvalue());
        //system.debug(ObjClass.deselectedFields);
        
        ObjClass.Next2();
        Objclass.back();
        ObjClass.getfilterList();
        ObjClass.previewReport();
        Objclass.addFilter();
        Objclass.removeFilter();
        
        ObjClass.selectedFilterField='LeadSource';
        ObjClass.selectedFilter='=';
        ObjClass.filterValue= ':sure';
        ObjClass.convertedCheck=true;
        ObjClass.Next3();
        
        ObjClass.getFrequencyList().get(1);
        system.debug('SELECTFriqncy======'+ObjClass.getFrequencyList().get(0));
        
        ObjClass.getDayList().get(1);
        ObjClass.getTimeList().get(0);
        ObjClass.emailIds='test@gmail.com';
        ObjClass.selectedUsers=user01.id;
        //ObjClass.getDays().get(0);
        system.debug('====getDays====='+ ObjClass.Days);
        ObjClass.getWeekList().get(1);
        ObjClass.getWeekList().get(3);
        //ObjClass.getDayNumberList().get(1);
        system.debug('===days==='+ObjClass.days);
        objclass.back();
        ObjClass.schedule();
        
        //Objclass.Next3();
        
        ObjClass.Next4();
        system.debug('===WEEK List======='+ ObjClass.getWeekList());
        //ssystem.debug('------SelectedTime-----'+ObjClass.sele )
        
       
       //String jobId = System.schedule('ReportGenerationNewCtrlTest', CRON_EXP, new ReportGenerationNewCtrl(sc));
     
        List<CronTrigger> ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger limit 1]; 
        if(ct.size() > 0){
            //System.assertEquals(0, ct[0].TimesTriggered); 
        }
        ObjClass.getWeekList().get(1);
        ObjClass.emailIds='test@gmail.com';
        ObjClass.selectedUsers=user01.id;
        
        ObjClass.getFrequencyList().get(0);
        system.debug('SELECTFriqncy======'+ObjClass.getFrequencyList().get(0));
        
        ObjClass.getDayList().get(1);
        ObjClass.getTimeList().get(0);
        ObjClass.emailIds='test@gmail.com';
        ObjClass.selectedUsers=user01.id;
        //ObjClass.getDays().get(0);
        system.debug('====getDays====='+ ObjClass.Days);
        ObjClass.getWeekList().get(1);
        ObjClass.getWeekList().get(3);
        //ObjClass.getDayNumberList().get(1);
        system.debug('===days==='+ObjClass.days);
        objclass.back();
        ObjClass.schedule();
        
        system.debug('schId======'+ObjClass.schId);
        Objclass.schId=ObjReport.id;
        ObjClass.selectedFinalFields.add(ObjClass.selectedFieldList[1].getvalue());
        system.debug('selectedFinalFields======'+ObjClass.selectedFinalFields);
        //.done();
        //ObjClass.moveUp();
    }
    Static testMethod void Case5()
    {
        List<Schedule_Report__c> ObjSchReport= new list<Schedule_Report__c>();
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User user01 = new User(Alias = 'Test001', Email='standarduser@testorg.com', 
                               EmailEncodingKey='UTF-8', LastName='Testlast', LanguageLocaleKey='en_US', 
                               LocaleSidKey='en_US', ProfileId = p1.Id, 
                               TimeZoneSidKey='America/Los_Angeles', UserName='user01@testorg.com');
        insert user01;
        
        Schedule_Report__c ObjReport = new Schedule_Report__c();
        ObjReport.Report_Name__c='Test';
        ObjReport.Selected_Fields__c='Lead: lastname,Account: name,Account: Phone,Lead: city,Lead: company,Contact:Lastname,Contact: Phone,Contact: Email,Lead: description';
        ObjReport.Labels__c='Labels';
        insert ObjReport;
        
        Schedule_Job__c schJob  = new Schedule_Job__c();
        schJob.Email_Ids__c = 'test@gmail.com';
        schJob.Schedule_Frequency__c = 'testFrequency';
        schJob.Scheduled_Job_Id__c = 'test123';
        schJob.Job_Name__c = 'testName';
        schJob.Next_Scheduled_Run__c = system.today();
        schJob.Submitted_By__c  = user01.id;
        schJob.Schedule_Report__c = ObjReport.id;
        insert schJob;
        
        PageReference pageRef = Page.ReportGenerationNew;
       
        ApexPages.currentPage().getParameters().put('Edit','false');
        ApexPages.currentPage().getParameters().put('schId',ObjReport.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(ObjReport);
        ReportGenerationNewCtrl ObjClass = new  ReportGenerationNewCtrl(sc);
        ObjClass.Next1();
        ObjClass.getfilterList();
        ObjClass.Next2();
        Objclass.back();
        ObjClass.filterValue='text';
        system.debug('====filterWrapperList====='+ObjClass.filterWrapperList);
        //ObjClass.Next3();
        // ObjClass.Next3();
        Objclass.back();
        ObjClass.previewReport();
        ObjClass.Next4();
        Objclass.back();
        Objclass.cancel();
       
        
    }
    
    
    
}