public class ScheduleJobTriggerHandler {
    public static void afterDelete(List<Schedule_Job__c> newList){
        abortJobs(newList);
    }
    
    public static void abortJobs(List<Schedule_Job__c> newList){
        for(Schedule_Job__c job :newList){
            if(job.Scheduled_Job_Id__c != null){
                try{
                    System.abortJob(job.Scheduled_Job_Id__c);
                }catch(exception e){
                    
                }
            }
        }
    }
}