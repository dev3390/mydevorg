@isTest
public class ScheduleJobTriggerHandlerTest {
    
    public static testmethod void test1(){
        
        Schedule_Report__c  schReport = new Schedule_Report__c ();
        schReport.Report_Name__c = 'test';
        insert schReport;
        
        Schedule_Job__c sc = new Schedule_Job__c();
        sc.Scheduled_Job_Id__c = 'test';
        sc.Schedule_Report__c = schReport.id;
        insert sc;
        
        Test.startTest();
        
        delete sc;
        
        Test.stopTest();
        
    }

}