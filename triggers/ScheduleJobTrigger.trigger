trigger ScheduleJobTrigger on Schedule_Job__c (after delete) {
    if(trigger.isAfter && trigger.IsDelete){
        ScheduleJobTriggerHandler.afterDelete(trigger.Old);
    }
}