trigger ReportTrigger on Schedule_Report__c (before delete) {
    if(trigger.IsBefore && trigger.isDelete){
        ReportTriggerHandler.beforeDelete(trigger.old);
    }
}